#!/usr/bin/env nextflow

process varscan2_somatic {
// Runs varscan2 on a set of paired samples
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(dataset) - Dataset
//     val(norm_run) - Normal Run Name
//     path(norm_bam) - Normal Alignment File
//     path(norm_bai) - Normal Alignment Index
//     val(tumor_run) - Tumor Run Name
//     path(tumor_bam) - Tumor Alignment File
//     path(tumor_bai) - Tumor Alignment Index
//   tuple
//     path(fa) - Reference FASTA
//     path(idx_files) - Reference Index Files
//     path(dict_file) - Reference Dict File
//   parstr - Additional Parameters
//
// output:
//   tuple
//     val(pat_name) - Patient Name
//     val(dataset) - Dataset
//     path('*vcf') - Variant Call File

// require:
//   NORM_TUMOR_BAMS_BAIS
//   REF_W_INDICES
//   params.varscan2$varscan2_somatic_parameters

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'varscan2_container'
  label 'varscan2_somatic'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/varscan2_somatic"


  input:
  tuple val(pat_name), val(dataset), val(norm_run), path(norm_pileup), val(tumor_run), path(tumor_pileup)
  val parstr
  val suffix

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*snp.vcf"), emit: snv_vcfs
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*indel.vcf"), emit: indel_vcfs

  script:
  """
  java -jar /opt/varscan/VarScan.jar somatic ${norm_pileup} ${tumor_pileup} ${dataset}-${pat_name}-${norm_run}_${tumor_run}${suffix} ${parstr}
  """
}
